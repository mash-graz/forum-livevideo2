# Restaurierte Fassung der FORUM Statdpark Live-Video-Streaming Lösung

## Die Quelle: Raspberry Pi

Während in der ursprünglichen Fassung ein RP3 mit 64bit Debian Linux genutzt wurde, verwende ich nun einen einfacheren RP2 und [HypriotOS](https://blog.hypriot.com/). Obwohl diese ältere Hardware weniger Rechenleistung bietet und auch auf Grund des 32bit-Befehlssatzes nicht mit der Effizienz der neueren Modelle mithalten kann, lässt sich hier leichter von den Raspberry-typischen Hardwarebescheunigungsfeatures beim Video-Encoding Gebrauch machen und die Installation gestaltet sich einfacher.

HypriotOS nutzt bei der Installation [cloud init](https://cloud-init.io/).  
Die gesamten Konfigurationseinstellungen, Angaben der zu installierenden Pakete etc., werden hier in einer zentralen YAML-Steuerdatei (`user-data`) aufgelistet, mit dem HypriotOS-Image auf die Speicherkarte geschrieben und dann beim ersten Start des RPIs automatisch abgearbeitet. Damit können notwendige Modifikationen viel übersichtlicher festgelgt und später nachvollzogen und reproduziert werden.

Das Kopieren auf die Speicherkarte kann entweder mit dem Befehl `dd` erfolgen, oder aber mit dem Hypriot-eigenen [flash-script](https://github.com/hypriot/flash), das die Sache noch einfacher macht.

    ./flash -u user-data -d /dev/sdX hypriotos-rpi-v1.9.0.img

### Softwareinstallation und Autostart

Neben der Möglichkeit, dem `cloud-init` in der Konfigurationsdatei (`user-data`) eine Liste zu installierenden Programmpaketen zu übergeben und auch die gesamte Software während der Installation auf den aktuellen Stand zu bringen, enthält HypriotOS von Haus aus einen Docker-Server, um komplexe Anwendungssoftware in fertig vorbereiteten Containern herunterzuladen und auszuführen. Da es aber im vorliegenden Anwendungsfall um eine eher trivialere Aufgabe geht, habe ich davon keinen Gebrauch gemacht, sondern dafür eine einfachere Lösung gewählt.

Ins Home-Verzeichnis des Standardbenutzers wird ein kleines shell-script installiert (/home/forum/rtmp-encoder.sh), das den Befehl zum Encodieren des video-streams enthält. 

Dies Script wird via systemd service automatisch ausgeführt und überwacht.

### Erreichbarkeit und Einloggen

HypriotOS nutzt Avahi, um die Gegenwart des RPIs im lokalen Netz bekannt zu machen. Es sollte daher ein einfaches:

    ssh forum@forum-live

genügen, um sich darauf einzuloggen.

## RTMP Server

Serverseitig wurde die Installation weitestgehend von einem älteren Projekt übernommen (https://gitlab.com/mur-at-public/live-video-verena).

Die Installation des entsprechenen Docker Containers am Server erfolgt dabei mit dem Befehl:

    docker pull registry.gitlab.com/mash-graz/forum-livevideo2

    docker run -d --restart unless-stopped \
    -p 8080:80 -p 1935:1935 \
    --mount 'type=volume,src=forum-livevideo2,dst=/tmp/video_archive' \
    --mount 'type=tmpfs,dst=/tmp/hls' \
    --name forum-livevideo2 \
    registry.gitlab.com/mash-graz/forum-livevideo2

## Web-Interface

Das Webinterface des Servers wird über die Nginx-Konfiguration und eine einzige HTML-Seite abgewickelt.

Neben der Hauptseite (gegenwärtig: http://kaffee.mur.at:8080 ) ist auch ein Archiv mit den Aufzeichnungen (http://kaffee.mur.at:8080/video_archiv/) verfügbar.