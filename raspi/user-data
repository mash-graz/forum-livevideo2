#cloud-config
# vim: syntax=yaml
#

# The current version of cloud-init in the Hypriot rpi-64 is 0.7.6
# When dealing with cloud-init, it is SUPER important to know the version
# I have wasted many hours creating servers to find out the module I was
# trying to use wasn't in the cloud-init version I had
# Documentation: http://cloudinit.readthedocs.io/en/0.7.9/index.html

# Set your hostname here, the manage_etc_hosts will update the hosts
# file entries as well
hostname: forum-live
manage_etc_hosts: true

# You could modify this for your own user information
users:
  - name: forum
    gecos: "forum technik"
    sudo: ALL=(ALL) NOPASSWD:ALL
    shell: /bin/bash
    groups: users,docker,video,input,audio,plugdev
    passwd: $5$nagelrost$UBP7JWH1bgpcstZhPzlAhTS9pHxaktr2.EnbpTsBO75
    lock_passwd: false
    ssh_pwauth: true
    chpasswd: {expire: false}

# # Set the locale of the system
# locale: "en_US.UTF-8"

# # Set the timezone
# # Value of 'timezone' must exist in /usr/share/zoneinfo
timezone: "Europe/Vienna"

# # Update apt packages on first boot
package_update: true
package_upgrade: true
package_reboot_if_required: true
# package_upgrade: false

# # Install any additional apt packages you need here
packages:
  - ffmpeg
  - v4l-utils
  - jed
  - iperf3
  - htop

write_files:
  - path: /home/forum/rtmp-encoder.sh
    permissions: 0755
    content: |
      #!/bin/sh
      ffmpeg \
      -thread_queue_size 512 -ar 22050 -f alsa  -ac 1 -i hw:1  \
      -thread_queue_size 512 -f v4l2 -framerate 20 -input_format yuyv422 \
      -video_size 752x416 -i /dev/video0 \
      -hide_banner -af aresample=async=1000 -pix_fmt yuv420p -ac 2 \
      -c:v h264_omx  -b:v 2M \
      -c:a aac -b:a 128k -bufsize 256 \
      -f flv rtmp://kaffee.mur.at:1935/live/forum

  - path: /lib/systemd/system/rtmp-encoder.service
    permissions: 0644
    content: |
      [Unit]
      Description=RTMP LiveStreaming Encoder
      ConditionFileIsExecutable=/home/forum/rtmp-encoder.sh
      After=network.eth0

      [Service]
      ExecStart=/bin/sh /home/forum/rtmp-encoder.sh
      Restart=always
      RestartSec=15

      [Install]
      WantedBy=multi-user.target

# These commands will be ran once on first boot only
runcmd:
  # Pickup the hostname changes
  - 'systemctl restart avahi-daemon'
  # Erhoehung der GPU Speicherzuweisung fuer das Hardwarencoding
  - "sed -i 's/gpu_mem=16/gpu_mem=128/' /boot/config.txt"
  # Autostart:
  - 'systemctl enable rtmp-encoder'

